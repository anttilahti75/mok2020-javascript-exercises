$(document).ready(function() {
    $("#myform").submit(function(e) {
        e.preventDefault();
        let number1 = $("#luku1").val();
        let number2 = $("#luku2").val();
        let sum = parseFloat(number1) + parseFloat(number2);
        $("#result").text("Summa on: " + sum);
    });
});