$(document).ready(function() {
    loadJSON();
});

function loadJSON() {
    $.ajax({
        url: "t04-talot.json",
        cache: false
    }).done(function(data) {
        console.log("done");
        naytaTalot(data);
    }).fail(function() {
        console.log("error");
    }).always(function() {
        console.log("complete");
    });
}

function naytaTalot(data) {
    $.each(data.houses, function(index, talo) {
        let cont = $('<div>').addClass('houseContainer');
        let image = $('<img/>').attr('src', talo.image).addClass('houseImage');
        let otsikko = $('<p>' + talo.address + '</p>').addClass('header');
        let koko = $('<p>' + talo.size + '</p>').addClass('text');
        let kuvaus = $('<p>' + talo.text + '</p>').addClass('text');
        let hinta = $('<p>' + talo.price + '</p>').addClass('text');
        cont.append(image);
        cont.append(otsikko);
        cont.append(koko);
        cont.append(kuvaus);
        cont.append(hinta);
        $('#houses').append(cont);
    });
}