$(document).ready(function() {
    
    $("#hakualue").autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "ajax-suggest.php",
                data: {
                    q: request.term
                },
                success: function(data) {
                    response(data.split('\t'));
                }
            });
        },
        focus: function(event, ui) {
            focusedItem(ui);
        }
    });
});

function focusedItem(obj) {
    let items = $(".ui-menu-item div")
    $.each(items, function(index, item) {
        if (item.textContent == obj.item.value) {
            item.style = "background-color: yellow";
        } else item.style = "background-color: ";
    });
}