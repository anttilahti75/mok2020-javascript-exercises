$(document).ready(function() {
    loadJSON();
    // let articles = 5;
    // for (let i = 0; i < articles; i++) {
    //     let artId = 'article' + i;
    //     let art = $('<article id="' + artId + '"></article>');
    //     let h3 = $('<h3>Article ' + artId + '</h3>');
    //     let p = $('<p>Text about article ' + artId + '</p>');
        
    //     h3.click(function() {
    //         p.toggle(200);
    //     });

    //     $('section').append(art);
    //     art.append(h3);
    //     art.append(p);
    // }
    // $("article").children("p").hide();
});

function loadJSON() {
    $.ajax({
        url: "https://jsonplaceholder.typicode.com/posts",
        cache: false
    }).done(function(data) {
        console.log("done");
        showNews(data);
    }).fail(function() {
        console.log("error");
    }).always(function() {
        console.log("complete");
    });
}

function showNews(data) {
    $.each(data, function(index, obj) {
        let title = $('<h3>' + obj.title + '</h3>')
            .click(function() {
                body.toggle(200);
            });
        let body = $('<p>' + obj.body + '</p>');
        let article = $('<article>')
        article.append(title);
        article.append(body);
        article.attr('id', index);
        $('section').append(article);
    });
    $("article").children("p").hide();
}

// function loadJSON() {
//     ajax("https://jsonplaceholder.typicode.com/posts", function(response) {
//         //console.log("response = " + response);
//         // create a json object
//         let JSONObject = JSON.parse(response);
//         for (var i=0;i<JSONObject.length;i++) {
//             let art = $('<article id="' + JSONObject[i].id + '"></article>');
//             let h3 = $('<h3>' + JSONObject[i].title + '</h3>');
//             let p = $('<p>' + JSONObject[i].body + '</p>');
//             p.css("display: inline");
//             h3.click(function() {
//                 p.toggle(200);
//             });
//             $('section').append(art);
//             art.append(h3);
//             art.append(p);
//         }
//         $("article").children("p").hide();
//     });
    
// }

// function ajax(url, fn) {
//     var req;
//     if (window.XMLHttpRequest) {
//         req = new XMLHttpRequest();
//     } else {
//         req = new ActiveXObject('Microsoft.XMLHTTP');
//     }
//     req.onreadystatechange = function() {
//         if (req.readyState == 4 && req.status == 200) {
//             fn(req.responseText);
//         }
//     }
//     req.open('GET', url, true);
//     req.send();
// }