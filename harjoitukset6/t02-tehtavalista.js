$(document).ready(function() {
    $("#addbutton").click(function() {
        let task = $("#task").val();
        if (task !== '') {
            $("#tasklist").append("<li>" + task + "</li>");

            $("#task").val("");
            $("#task").focus();
    
            let span = $("<span> X </span>").css("color", "red");
            span.on("click", function() {
                $(this).parent().remove();
            });
            $("#tasklist").children("li:last-child").append(span);
        }

    });
});