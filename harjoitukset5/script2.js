var searchURLResults   = [];
var index              = 0;
var maxResults         = 0;
var resultURL          = "";
let ul_nimet = document.getElementById("nimet");


function checkInput(event) {
    let keyCode = event.keyCode;
    if(keyCode == 40) {
        browse_names(keyCode);
    } else {
        haeNimet(document.getElementById("nimi_kentta").value);
    }
}
function haeNimet(str) {
    if (str.length == 0) {
        empty_node_list(ul_nimet);
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                empty_node_list(ul_nimet);
                let resp = this.responseText;
                let patt = new RegExp(`^${str}`, 'i');
                if (patt.test(resp) == true) {
                    // console.log(`Tasmaa, haku = ${patt}, vastaus =  ${this.responseText}`);
                    let searchPlainResults = resp.split('\t');
                    for (let name of searchPlainResults) {
                        let linode = document.createElement('li');
                        let litext = document.createTextNode(name);
                        linode.addEventListener('click', show_name, false);
                        linode.appendChild(litext);
                        ul_nimet.appendChild(linode);
                    }
                } else {
                    // console.log(`Ei tasmaa, haku = ${patt}, vastaus =  ${this.responseText}`);
                    empty_node_list(ul_nimet);
                }
            }
        };
        xmlhttp.open("GET", "ajax-suggest.php?q=" + str, true);
        xmlhttp.send();
    }
}

function empty_node_list(node) {
    while (node.hasChildNodes()) {
        node.removeChild(node.firstChild);
    }
}

function browse_names(keyCode) {
    let name_list = [null];
     // 40 = Down, 38 = Up, 13 = Enter, 27 = Esc
    if (ul_nimet.hasChildNodes()) {
        name_list.push(...ul_nimet.querySelectorAll('li'));
        maxResults = name_list.length;
        if(keyCode == 40) {
            if (index == 0) {
                index++;
                name_list[index].style.backgroundColor = 'limegreen';
            } else if (index < maxResults - 1) {
                index++;
                name_list[index - 1].style.backgroundColor = '';
                name_list[index].style.backgroundColor = 'limegreen';
            } else if (index == maxResults - 1) {
                name_list[index - 1].style.backgroundColor = '';
                name_list[index].style.backgroundColor = 'limegreen';
            }
            console.log(index);
            return;
        } else if(keyCode == 38) {
            if (index == 0) {
                name_list[index + 1].style.backgroundColor = '';
            } else if (index == 1) {
                index--;
                name_list[index + 1].style.backgroundColor = '';
            } else if (index > 1) {
                index--;
                name_list[index + 1].style.backgroundColor = '';
                name_list[index].style.backgroundColor = 'limegreen';
            }
            console.log(index);
            return;
        } else if(keyCode == 13) {
            let nimikentta = document.getElementById("nimi_kentta");
            nimikentta.value = "";
            nimikentta.value = name_list[index].textContent;
            try {
                var xhr = new XMLHttpRequest();
            } catch(e) {
                alert(e);
            }
            console.log(xhr);
        } else if(keyCode == 27) {
            //field.value = "";
        }
    }
}

function show_name(event) {
    //console.log(event.target.textContent);
    let name = event.target.textContent
    let name_node = document.createElement('p');
    let name_text = document.createTextNode(name);
    b = document.getElementsByTagName('body');
    while (b[0].hasChildNodes()) {
        if (b[0].firstChild == 'script') {
            return;
        } else {
            b[0].removeChild(b[0].firstChild);
        }
    }
    name_node.appendChild(name_text);
    b[0].appendChild(name_node);
}