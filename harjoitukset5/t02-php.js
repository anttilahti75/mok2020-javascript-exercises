var index              = 0;
var maxResults         = 0;
let ul_nimet = document.getElementById("nimet");
let nimikentta = document.getElementById("nimi_kentta");
nimikentta.value = '';

function checkInput(event)
{
    let keyCode = event.keyCode;
    if(keyCode == 40 || keyCode == 38 || keyCode == 13 || keyCode == 27)
    {
        browse_names(keyCode);
        return;
    }
    else if ((keyCode >= 65 && keyCode <= 90) || keyCode == 222 || keyCode == 219 || keyCode == 59)
    {
        index = 0;
        console.log("ajax fetch: " + ajax_fetch("input", nimikentta.value));
        return;
    }
}

function ajax_fetch(src, str)
{
    let show_name;

    switch(src)
    {
        case 'input':
        case 'enter':
            show_name = false;
            break;
        case 'click':
        case 'button':
            show_name = true;
            break;
    }

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200 && show_name == false)
        {
            console.log(listNames(str, this.responseText));
        }
        else if (this.readyState == 4 && this.status == 200 && show_name == true)
        {
            document.write(this.responseText);
        }
    }

    xmlhttp.open("GET", "ajax-suggest.php?q=" + str, true);
    xmlhttp.send();
    return "done";
}

function listNames(str, resp)
{
    empty_node_list(ul_nimet);
    let patt = new RegExp(`^${str}`, 'i');

    if (patt.test(resp) == true)
    {
        //console.log(`Tasmaa, haku = ${patt}, vastaus =  ${resp}`);
        let searchPlainResults = resp.split('\t');

        for (let name of searchPlainResults)
        {
            let linode = document.createElement('li');
            let litext = document.createTextNode(name);
            linode.addEventListener('click', function() { ajax_fetch('click', name); }, false);
            linode.appendChild(litext);
            ul_nimet.appendChild(linode);
        }
        return "Namelist created";
    }
    else
    {
        //console.log(`Ei tasmaa, haku = ${patt}, vastaus =  ${resp}`);
        empty_node_list(ul_nimet);
        return "Nothing found, namelist emptied";
    }
}

function empty_node_list(node)
{
    while (node.hasChildNodes())
    {
        node.removeChild(node.firstChild);
    }
}

function browse_names(keyCode) {
    let name_list = [null];

    if (ul_nimet.hasChildNodes()) {
        name_list.push(...ul_nimet.querySelectorAll('li'));
        maxResults = name_list.length;

        if(keyCode == 40) { // Down
            if (index == 0)
            { // "Selection" is at 0 (null)
                index++;
                name_list[index].style.backgroundColor = 'limegreen';
            }
            else if (index < maxResults - 1)
            { // "Selection" is NOT at the end
                index++;
                name_list[index - 1].style.backgroundColor = '';
                name_list[index].style.backgroundColor = 'limegreen';
            }
            else if (index == maxResults - 1 && maxResults == 2)
            { // There is only one real result so don't move the selection
                name_list[index].style.backgroundColor = 'limegreen';
            }
            else if (index == maxResults - 1)
            { // "Selection" is at the end
                name_list[index - 1].style.backgroundColor = '';
                name_list[index].style.backgroundColor = 'limegreen';
            }
            console.log("selection: " + index);
            return;
        }
        else if(keyCode == 38)
        { // Up
            if (index == 0)
            { // "Selection" is at 0 (null)
                name_list[index + 1].style.backgroundColor = '';
            }
            else if (index == 1)
            { // "Selection" is at the beginning
                name_list[index].style.backgroundColor = 'limegreen';
            }
            else if (index > 1)
            { // "Selection" is NOT at the beginning
                index--;
                name_list[index + 1].style.backgroundColor = '';
                name_list[index].style.backgroundColor = 'limegreen';
            }
            console.log("selection: " + index);
            return;
        }
        else if(keyCode == 13)
        { // Enter
            nimikentta.value = "";
            nimikentta.value = name_list[index].textContent;
            index = 0;
            ajax_fetch('enter', nimikentta.value);
            return;
        }
        else if(keyCode == 27)
        { // ESC
            nimikentta.value = "";
            index = 0;
            empty_node_list(ul_nimet);
            return;
        }
    }
}