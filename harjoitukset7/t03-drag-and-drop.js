function allowDrop(event) {
    event.preventDefault();
}

function drop(event) {
    event.preventDefault();

    var files = event.dataTransfer.files;
    var filesCount = files.length;

    for (var i = 0; i < filesCount; i++) {
        var file = files[i];
        if (!file.type.match('image.*')) continue;
        var fileReader = new FileReader();
        fileReader.onload = function(e) {
            var img = document.createElement("img");
            img.setAttribute("src", e.target.result);
            img.setAttribute("class", "thumbnail");
            var div = document.getElementById("divToDrop");
            div.appendChild(img);
        }
        fileReader.readAsDataURL(file);
    }
}


