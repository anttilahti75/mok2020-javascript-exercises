// Powerup
function Powerup(ctx,x,y, type) {
	this.ctx = ctx;
	this.w = 20;
	this.h = 20;
	this.x = x;
    this.y = y;
    this.yspeed = 2;
    this.type = type;

	// color of powerup
    this.color = "rgb(100, 0, 0)";
    
	// draw a powerup
	this.draw = function() {	
		ctx.fillStyle = this.color;
		ctx.fillRect(this.x,this.y,this.w,this.h);
    }
    
    // move a powerup
	this.move = function() {
		this.y += this.yspeed;
		if (this.y > this.canvasHeight) {
			return true;
		}
		return false;
	}
	
}